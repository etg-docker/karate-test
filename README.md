# etg-docker: ekleinod/karate-test

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/karate-test/)
- **Docker hub:** [ekleinod/karate-test](https://hub.docker.com/r/ekleinod/karate-test)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/karate-test/), [issue tracker](https://gitlab.com/etg-docker/karate-test/-/issues)


## Supported tags

- `1.0.0`, `1.0.0-1.1.0`, `latest`


## What is this image?

This is a docker image for automated testing with karate.
The image has the name `ekleinod/karate-test`.

It uses the karate standalone jar.
It uses the current OpenJDK image as base, the OpenJDK version is not used in tags.

- [Karate](https://github.com/karatelabs/karate)
- [Standalone JAR](https://github.com/karatelabs/karate/tree/master/karate-netty#standalone-jar)
- [OpenJDK](https://hub.docker.com/_/openjdk)

For the complete changelog, see [changelog.md](changelog.md)


## How to use the image

You can use the image as follows:

~~~ bash
docker run --rm --name <containername> --user "$(id -u):$(id -g)" --volume "${PWD}":/usr/src/myapp ekleinod/karate-test <karate-tests>
~~~

Please note that `/usr/src/myapp` is the work directory of the image.

For a simple test showing the karate-test help page, run

~~~ bash
$ docker run --rm --name karate-test ekleinod/karate-test
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_hello-world.sh
~~~


## Releases

The latest release will always be available with:

`ekleinod/karate-test:latest`

There are two naming schemes:

1. `ekleinod/karate-test:<internal>-<karate-test>`

	Example: `ekleinod/karate-test:1.0.0-1.1.0`

2. internal version number `ekleinod/karate-test:<major>.<minor>.<patch>`

	Example: `ekleinod/karate-test:1.0.0`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2022-2022 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

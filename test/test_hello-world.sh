source ../settings.sh

echo "Testing hello world."
echo

docker run \
  --rm \
  --user "$(id -u):$(id -g)" \
  --name $CONTAINERNAME \
  --volume "${PWD}/:/usr/src/myapp" \
  $IMAGENAME \
    "tests/hello-world.feature" \
    --output "results"

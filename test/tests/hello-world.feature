﻿Feature: Hello World - using https://jsonplaceholder.typicode.com

Background:
	* url "https://jsonplaceholder.typicode.com"
	* def api_path = "users"


Scenario: just call the API
	Given path api_path

	When method get

	Then assert responseStatus == 200
	And match responseType == 'json'

	And match response == '#[10]'
